<?php

namespace App\Controller\Admin;
use App\Entity\Concert;
use App\Entity\Artiste;
use App\Entity\Email;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
// configuration espace admin
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;

use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
         $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        return $this->redirect($adminUrlGenerator->setController(ConcertCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }
    public function configureActions(): Actions
    {
        $actions = parent::configureActions();

        $actions->update(Crud::PAGE_INDEX, 'edit', function (Action $action) {
            return $action
                ->setIcon('fa fa-edit') 
                ->setLabel('Edit');  
        });

        $actions->update(Crud::PAGE_INDEX, 'delete', function (Action $action) {
            return $action
                ->setIcon('fa fa-trash')
                ->setLabel('Delete');
        });

        return $actions;
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Espace admin : Symfony Tp4');
    }


    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');

        yield MenuItem::linkToCrud('Les concerts', 'fas fa-volume-high', Concert::class);
        yield MenuItem::linkToCrud('Les concerts passés', 'fas fa-volume-low', Concert::class);

        yield MenuItem::linkToCrud('Les artistes', 'fas fa-user', Artiste::class);
        yield MenuItem::linkToCrud('Utilisateur', 'fas fa-users', User::class);

        yield MenuItem::linkToCrud('Newsletter', 'fas fa-paper-plane', Email::class);


        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
    }
}
